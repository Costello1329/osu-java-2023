import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double p = scanner.nextDouble();
        double sum = 0.;

        for (int i = 0; i < n; ++ i) {
            double xi = scanner.nextDouble();
            /// abs(x) -> |x|        (absolute value)
            /// pow(x, p) -> x ^ p   (power)
            sum += Math.pow(Math.abs(xi), p);
        }

        sum = Math.pow(sum, 1. / p);
        System.out.println(sum);
    }
}
