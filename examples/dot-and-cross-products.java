import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите 2 вектора в виде пар x, y: ");
        final double x1 = scanner.nextDouble();
        final double y1 = scanner.nextDouble();
        final double x2 = scanner.nextDouble();
        final double y2 = scanner.nextDouble();
        System.out.print("Скалярное произведение: ");
        System.out.println(x1 * x2 + y1 * y2);
        System.out.print("Векторное произведение: ");
        System.out.println(x1 * y2 - y1 * x2);
    }
}
