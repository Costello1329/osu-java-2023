import java.util.function.Function;

public class Main {
    public static void main (String[] args) {
        final Function<Double, Long> round = Math::round;
        final Function<Long, Boolean> isGreaterOrEqualThanThree = x -> x >= 3.;
        final Function<Double, Boolean> composition = compose(isGreaterOrEqualThanThree, round);
        System.out.println(composition.apply(2.4));
        System.out.println(composition.apply(2.7));

        final Function<Double, Boolean> andThenResult = andThen(round, isGreaterOrEqualThanThree);
        System.out.println(andThenResult.apply(2.4));
        System.out.println(andThenResult.apply(2.7));
    }

    public static <A, B, C> Function<A, C> compose (
        final Function<? super B, ? extends C> after,
        final Function<? super A, ? extends B> before
    ) {
        return (final A x) -> after.apply(before.apply(x));
    }

    public static <A, B, C> Function<A, C> andThen (
        final Function<? super A, ? extends B> before,
        final Function<? super B, ? extends C> after
    ) {
        return (final A x) -> after.apply(before.apply(x));
    }

    public static <T> Function<T, T> identity () {
        return (final T t) -> t;
    }
}
