public final class ComplexNumber {
    private final double re;
    private final double im;

    public ComplexNumber (double re, double im) {
        this.re = re;
        this.im = im;
    }

    public double getRe () {
        return re;
    }

    public double getIm () {
        return im;
    }
    
    @Override
    public boolean equals (Object other) {
        if (other instanceof ComplexNumber) {
            ComplexNumber otherComplexNumber = (ComplexNumber)other;
            return re == otherComplexNumber.re && im == otherComplexNumber.im;
        }
        
        else
            return false;
    }

    @Override
    public int hashCode () {
        long real = Double.doubleToLongBits(re);
        long imag = Double.doubleToLongBits(im);
        if (real == 1L << 63) real = 0;
        if (imag == 1L << 63) imag = 0;
        long h = real ^ imag;
        return (int)h ^ (int)(h >>> 32);
    }
}